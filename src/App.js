import React, { useState } from 'react';
import './App.css';

function App() {
  const [count, setCount] = useState(0);

  const countPlus5 = () => {
    setCount(count + 5);
  }
  const countMin5 = () => {
    setCount(count - 5);
  }



  return (
    <div className="App">
      {count}
      <div class="d-grid gap-2 d-md-block">
        <button onClick={() => setCount(count + 1)} class="btn btn-primary" type="button">Increment</button>
        <button onClick={() => setCount(count - 1)} class="btn btn-primary" type="button">Decrement</button>
      </div>
      <div class="d-grid gap-2 d-md-block">
        <button onClick={() => countPlus5()} class="btn btn-primary" type="button">Increment 5</button>
        <button onClick={() => countMin5()} class="btn btn-primary" type="button">Decrement 5</button>
      </div>


    </div>
  );
}

export default App;
